const proxy = require("http-proxy-middleware");
 
module.exports = function(app) {
  app.use(
    proxy("/test", {
      target: "http://39.96.177.29",
      changeOrigin: true,
      pathRewrite: {
        '^/test': ''
      }
    })
  );
};