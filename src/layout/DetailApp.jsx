import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import Detail from '@/pages/Detail.jsx';
import ArticalDetail from '@/pages/ArticalDetail.jsx';
import ZaocanDetail from '@/pages/ZaocanDetail.jsx';
import SearchDetail from '@/pages/SearchDetail.jsx';
class Com extends Component {
  render () {
    return (
      <Switch>
        <Route path = "/detail/articalDetail/:id" component = { ArticalDetail } />
        <Route path = "/detail/zaocanDetail/:id" component = { ZaocanDetail } />
        <Route path = "/detail/searchDetail/:id" component = { SearchDetail } />
        <Route path = "/detail/:id" component = { Detail } />
      </Switch>
    )
  }
}

export default Com 
