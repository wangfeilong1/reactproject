import Swiper from 'swiper/dist/js/swiper.js'
import 'swiper/dist/css/swiper.min.css'
import React from 'react';
import store from '@/store';
import './style.scss';

class Banner extends React.Component {
  
  state = {
    data: []
  }
  componentDidMount() {
    // simulate img loading
    setTimeout(() => {
      this.setState({
        data: store.getState().homeStore.bannerdata
      });
    }, 100);      
  }
  componentDidUpdate(){
    new Swiper ('.swiper-container11', {
      // direction: 'vertical', // 垂直切换选项
      loop: true, // 循环模式选项
      
      // 如果需要分页器
      pagination: {
        el: '.swiper-pagination',
      },
      autoplay: {
        delay: 2500,
        disableOnInteraction: false,
      }
    })  
  }
  render() {
    return (
      <div className="swiper-container swiper-container11">
      <div className="swiper-wrapper">
        {
          this.state.data.map((val,index)=>{
            return (
              
              <div className="swiper-slide" key={index}>
              <div  className="banner"><img src={val.pic1} alt=""/><span>{val.title1}</span></div>
              <div  className="banner"><img src={val.pic2} alt=""/><span>{val.title2}</span></div>
              <div  className="banner"><img src={val.pic3} alt=""/><span>{val.title3}</span></div>
              <div className = "banner_comment"><h1>{val.type}</h1><h2>{val.comment}</h2></div>
              </div>
            )
          })
        }
    </div>
    <div className="swiper-pagination"></div>
</div>
    );
  }
}


export default Banner
