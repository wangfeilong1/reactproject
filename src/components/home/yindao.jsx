import Swiper from 'swiper/dist/js/swiper.js'
import 'swiper/dist/css/swiper.min.css'
import React from 'react';
import './style.scss';

class yindao extends React.Component {
  deleteLS(){
    localStorage.setItem('isFirst','not');
    this.forceUpdate();
  }
  componentDidMount(){
    new Swiper ('.swiper-container-yd', {
      direction: 'vertical', // 垂直切换选项
      loop: false, // 循环模式选项
    })  
  }
  render() {
    return (
      <div className="swiper-container swiper-container-yd">
      <div className="swiper-wrapper swiper-wrapper-yd">
              <div className="swiper-slide swiper-slide-yd" key="1">
                <div className="yindao"><img src={'http://img.zcool.cn/community/01677559c5ef22a801218e181c065c.jpg@1280w_1l_2o_100sh.jpg'} alt=""/><span>1</span></div>
              </div>
              <div className="swiper-slide swiper-slide-yd" key="2">
                <div  className="yindao"><img src={'http://img1.imgtn.bdimg.com/it/u=4065223183,2292765616&fm=15&gp=0.jpg'} alt=""/><span>2</span></div>
              </div>
              <div className="swiper-slide swiper-slide-yd" key="3">
                <div  className="yindao"><img src={'http://img.redocn.com/sheji/20170425/xiajiqingshuangpijiujiemeishijiehuodonghaibao_8135401.jpg.285.jpg'} alt=""/><span>3</span></div>
              </div>
              <div className="swiper-slide swiper-slide-yd" key="4">
                <div  className="yindao"><img src={'http://img.zcool.cn/community/012f8a5a066372a801204a0edf1ea8.jpg@1280w_1l_2o_100sh.jpg'} alt=""/><span>4</span>
                  <a href="/#/home" onClick={this.deleteLS.bind(this)} >马上进入美食节</a>
                </div>
              </div>
    </div>
    <div className="swiper-pagination"></div>
</div>
    );
  }
}


export default yindao
