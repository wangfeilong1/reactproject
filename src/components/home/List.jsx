import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import './style.scss';

const List = ({ list }) => (
  <ul className="prolist">
  {
    list.map((item, index) => {
    return (
    <Link className="toDetail" to = { '/detail/' + item.id } key = {item.id}>
    <li className="index_list" key = {item.id}>
      <div className="proimg">
        <img className="listImg" src={ item.img } alt={item.img}/>
      </div>
      <div className="info">
       {item.title}
      </div>
      </li>
      </Link>
    )
  })
}
  </ul>
)

List.propTypes = {
  list: PropTypes.array
}

export default List

