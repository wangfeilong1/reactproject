import { ImagePicker } from 'antd-mobile';
import React from 'react';
// import ReactDOM  from 'react-dom';

const data = [{
  url: "https://s1.c.meishij.net/images/default/tx2_8.png",
  id: '2121',
}, {
  url: 'https://zos.alipayobjects.com/rmsportal/hqQWgTXdrlmVVYi.jpeg',
  id: '2122',
}];

class ImagePickerExample extends React.Component {
  state = {
    files: data,
  };
  onChange = (files, type, index) => {
    // console.log(files, type, index);
    
    this.setState({
      files,
    });
    localStorage.setItem('avater',JSON.stringify({avater:this.state.files[1].url}))
  };
  onAddImageClick = (e) => {
    e.preventDefault();
    this.setState({
      files: this.state.files.concat({
        url: 'https://zos.alipayobjects.com/rmsportal/hqQWgTXdrlmVVYi.jpeg',
        id: '3',
      }),
    });
  };
  onTabChange = (key) => {
    console.log(key);
  };
  render() {
    const { files } = this.state;
    return (
      <div>
        <ImagePicker
          files={files}
          onChange={this.onChange}
          onImageClick={(index, fs) => console.log(index, fs)}
          selectable={files.length < 5}
          onAddImageClick={this.onAddImageClick}
        />
      </div>
    );
  }
}

export default ImagePickerExample;

