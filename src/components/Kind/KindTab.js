import React, {Component} from 'react';

import './style.scss';
// import KindList from "./KindList";


class KindTab extends Component {
    constructor(props){
        super(props);
        this.state={
            currentIndex:0
        }
    }
    //组件状态中的currentIndex的值改变为当前选中的元素的index
    changeIndex(index){
        this.setState({
            currentIndex:index
        })
    }
    //根据当前选中元素的索引给元素添加动态class
    addtitleClass(index){
        
        return this.state.currentIndex===index? 'nav-tab-active-show':'nav-tab-active-cont'
    }
    addListClass(index){
        return this.state.currentIndex===index? 'nav-list-active-show':'nav-list-active-cont'
    }
    render() {
        return (
            <div className={"kind-tab"}>
                <div className={"kind-tab-title"}>
                    {/*React.Children方法拿到所有的children中的元素*/}
                        {React.Children.map(this.props.children, (element, index) => (
                            <ul
                                onClick={this.changeIndex.bind(this,index)}
                                className={this.addtitleClass(index)}
                            >{element.props.title}</ul>
                        ))}
                </div>
                <div className={"kind-tab-content"}>
                    {React.Children.map(this.props.children, (element, index) => (
                        <div className={this.addListClass(index)} >{element}</div>
                    ))}
                </div>
            </div>
        );
    }
}

export default KindTab;

