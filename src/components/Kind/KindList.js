import React, {Component} from 'react';
import api from '@/api/Kind';


import './style.scss'

class KindList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            list: []
        }
    }

    componentDidMount() {
        let type = this.props.type;
        api.reuuireList(type).then(data => {
            this.setState({
                list: data.data
            })
        })
    }

    render() {
        let { history }=this.props;
        return (
            <div className={"kind-list"}>
                <ul>
                    {this.state.list && this.state.list.map((item, index) => (
                        <li key={index} onClick={()=>history.push(`./detail/searchDetail/${item.id}`)}>
                            <img src={item.image} alt=""/>
                            <span>{item.title}</span>
                        </li>
                    ))}
                </ul>
            </div>
        );
    }
}

export default KindList;