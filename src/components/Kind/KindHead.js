import React, {Component} from 'react';

import './style.scss';


class KindHead extends Component {
    render() {
        let { history }=this.props;
        return (
            <div className={"kind-head"}>
                <i className={"iconfont icon-back kind-head-back"} onClick={()=>{ history.push("./home") }}></i>
                <div className={"kind-search"}  onClick={ ()=>{history.push('./search')}}>
                    <i className={"iconfont icon-huaban"}></i>
                <input type="text" placeholder={"搜索任何你想要的"} disabled className={"kind-head-search-text"}/>
                </div>
            </div>
        );
    }
}

export default KindHead;
