import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import api from '@/api/artical';
import './style.scss';

class ArticalList extends Component{
  constructor(props){
    super(props)
  this.state = {
    list:[]
  }
}
  componentDidMount () {
    api.requestData().then(data => {
      this.setState({
        list: data
      })
    })
  }
render(){
  return (
    <ul className="Articallist">
    {
      this.state.list.map((item, index) => {
      return (
      <Link className="toArticalDetail" to = { '/detail/articalDetail/' + item.id } key = {item.id}>
      <li key = {item.id}>
      <div className="artical_content">
        <div className="articalInfo">
        {item.title}
        </div>
        <img className="articalImg" src={ item.img } alt={item.img}/>
        </div>
        <div className="artical_view">
        <img src={"https://s1.st.meishij.net/p2/20181214/20181214154020_569.png"} alt=""/>
          <span>{parseInt(Math.random()*10000)}</span>
          <p>{2018}-{parseInt(Math.random()*12)}-{parseInt(Math.random()*30)}</p>
        </div>
        </li>
        </Link>
        )
      })
    }
    </ul>
    )
  }
}


export default ArticalList