import React from 'react';
import './style.scss';

const DetailHead = props => (
    <div className={"detail-head"}>
        <div className={"detail-head-back"} onClick={()=>{props.history.push('/home')}}>
            <i className={"iconfont icon-back"} />
            <span>美食杰</span>
        </div>
        <div className={"detail-head-search"} onClick={()=>{props.history.push('/search')}} >
            <i className={"iconfont icon-huaban"} />
        </div>
        <div className={"detail-head-kind"} onClick={()=>{props.history.push('/kind')}}>
            <span>菜谱分类</span>
            <i className={"iconfont icon-row-right"} />
        </div>
    </div>
);
export default DetailHead;
