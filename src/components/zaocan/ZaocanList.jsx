import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import api from '@/api/zaocan';
import './style.scss';

class zaocanList extends Component{
  constructor(props){
    super(props)
  this.state = {
    list:[]
  }
}
  componentDidMount () {
    api.requestData().then(data => {
      this.setState({
        list: data
      })
    })
  }
render(){
  return (
    <ul className="zc_list">
    {
      this.state.list.map((item, index) => {
      return (
      <Link className="toZaocanDetail" to = { '/detail/zaocanDetail/' + item.id } key = {item.id}>
      <li key = {item.id}>
      <div className="zc_content">
      <img className="zaocanImg" src={ item.image } alt={item.image}/>
        <div className="zaocanInfo">
          <p>{item.title}</p><br/>
        <div className="zaocan_view">
        <div className="stars"><i></i><i></i><i ></i><i></i><i></i></div>
          <span>{item.pingjia}</span>
        </div>
        </div>
        </div>
        
        </li>
        </Link>
        )
      })
    }
    </ul>
    )
  }
}


export default zaocanList