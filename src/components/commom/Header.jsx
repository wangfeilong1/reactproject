import React from 'react';
import { Link } from 'react-router-dom';
import './style.scss';

import { Popover, NavBar, Icon } from 'antd-mobile';

const Item = Popover.Item;

const myImg = src => <img src={`https://gw.alipayobjects.com/zos/rmsportal/${src}.svg`} className="am-icon am-icon-xs" alt="" />;
class Com extends React.Component {
  state = {
    visible: true,
    selected: '',
  };
  onSelect = (opt) => {
    this.setState({
      visible: false,
      selected: opt.props.value,
    });
  };
  handleVisibleChange = (visible) => {
    this.setState({
      visible,
    });
  };
  goSearch(){
    window.location.href="/#/search";
  }
  render() {
    return (<div className="header">
    
      <NavBar
        mode="light"
        rightContent={
          <Popover 
            overlayClassName="fortest"
            overlayStyle={{ color: 'currentColor' }}
            visible={this.state.hidden}
            overlay={[
              (<Item key="4" value="scan" icon={myImg('tOtXhkIWzwotgGSeptou')} data-seed="logId">Scan</Item>),
              (<Item key="5" value="special" icon={myImg('PKAgAqZWJVNwKsAJSmXd')} style={{ whiteSpace: 'nowrap' }}>My Qrcode</Item>),
              (<Item key="6" value="button ct" icon={myImg('uQIYTFeRrjPELImDRrPt')}>
                <span style={{ marginRight: 5 }}>Help</span>
              </Item>),
            ]}
            align={{
              overflow: { adjustY: 0, adjustX: 0 },
              offset: [-10, 0],
            }}
            onVisibleChange={this.handleVisibleChange}
            onSelect={this.onSelect}
          >
            <div style={{
              height: '100%',
              padding: '0 15px',
              marginRight: '-15px',
              display: 'flex',
              alignItems: 'center',
            }}
            >
              <Icon type="ellipsis" />
            </div>
          </Popover>
        }
      >
      <div className="message">
        <Link to="/userapp/login">
        <svg viewBox="0 0 1027 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="2517"><path d="M1025.631 317.162c0.002-0.1 0.002-0.199 0.002-0.3l0-46.471-1.254-3.683c-13.853-40.689-46.175-66.979-82.341-66.979L81.968 199.729c-18.908 0-38.212 5.804-52.964 15.923-18.704 12.83-29.004 31.316-29.004 52.054l0 481.568c0 42.391 42.384 70.867 81.968 70.867L942.038 820.141c39.987 0 69.107-30.795 81.893-57.167l2.354-4.854L1026.285 322.687C1026.284 320.784 1026.058 318.934 1025.631 317.162zM526.531 495.854c-8.833 5.669-20.222 5.674-29.056 0.01l-88.244-57.875c-0.022-0.017-0.047-0.03-0.071-0.047L117.595 246.72l788.806 0L526.531 495.854zM46.989 749.273 46.989 267.705c0-3.182 1.304-6.145 3.515-8.789L357.688 460.382 47.025 750.052C47.01 749.792 46.989 749.535 46.989 749.273zM398.143 486.912l73.641 48.299 0.147 0.095c12.17 7.853 26.114 11.776 40.064 11.776 13.946 0 27.896-3.926 40.065-11.775l79.249-51.975 307.465 289.818L91.159 773.15 398.143 486.912zM671.708 456.837l297.348-195.012c3.976 4.977 7.227 10.75 9.585 16.642l0 38.395c0 1.903 0.226 3.753 0.653 5.526-0.001 0.1-0.002 0.2-0.002 0.301l0 424.075L671.708 456.837z" p-id="2518"></path></svg>
        </Link>
      </div>
        <div className="search"><img src="http://static.meishij.net/wap6/images/topsearchicon@3x.png" alt=""/><input type="text" onFocus={this.goSearch.bind(this)} placeholder="请输入你感兴趣的"/></div>
      </NavBar>
    </div>);
  }
}

export default Com;