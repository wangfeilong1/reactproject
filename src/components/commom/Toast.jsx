import React from 'react';
import { Toast, WhiteSpace, WingBlank, Button } from 'antd-mobile';

function showToast() {
  Toast.info('This is a toast tips !!!', 1);
}

function showToastNoMask() {
  Toast.info('Toast without mask !!!', 2, null, false);
}

function successToast() {
  Toast.success('Load success !!!', 1);
}

function failToast() {
  Toast.fail('Load failed !!!', 1);
}

function offline() {
  Toast.offline('Network connection failed !!!', 1);
}

function loadingToast() {
  Toast.loading('Loading...', 1, () => {
    console.log('Load complete !!!');
  });
}



class ToastExample extends React.Component {
  componentDidMount() {
    Toast.loading('Loading...', 30, () => {
      console.log('Load complete !!!');
    });

    setTimeout(() => {
      Toast.hide();
    }, 3000);
  }
  render() {
    return (
      <WingBlank>
        <WhiteSpace />
        <Button onClick={showToast}>text only</Button>
        <WhiteSpace />
        <Button onClick={showToastNoMask}>without mask</Button>
        <WhiteSpace />
        <WhiteSpace />
        <Button onClick={successToast}>success</Button>
        <WhiteSpace />
        <Button onClick={failToast}>fail</Button>
        <WhiteSpace />
        <Button onClick={offline}>network failure</Button>
        <WhiteSpace />
        <Button onClick={loadingToast}>loading</Button>
        <WhiteSpace />
      </WingBlank>
    );
  }
}

export default ToastExample;