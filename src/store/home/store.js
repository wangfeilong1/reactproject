const reducer = (state = {
  list: [],
  bannerdata: []
}, action) => {
  const { type, data } = action; // type表示你要做的事行为，data就是传递过来的数据
  switch (type) {
    case 'CHANGE_BANNER_DATA':
      return {
        list: state.list,
        bannerdata: data
      }
    case 'CHANGE_LIST_DATA':
      return {
        list: data,
        bannerdata: state.bannerdata
      }
    default:
      return state;
  }
}

export default reducer;
