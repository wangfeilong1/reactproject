import api from '@/api/home';

const requestData = () => (dispatch, getState) => {
  api.requestData().then(data => {
    dispatch({type: 'CHANGE_LIST_DATA', data})
  })
}
const requestBannerData = () => (dispatch, getState) => {
  api.requestBannerData().then(data => {
    dispatch({type: 'CHANGE_BANNER_DATA', data})
  })
}

export default {requestData, requestBannerData};
