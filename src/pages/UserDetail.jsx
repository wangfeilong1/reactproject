import React,{ Component } from 'react';
// import { $ } from 'jquery';
import './UserDetail.scss';
import ImagePickerExample from '@/components/user/imgpick.jsx'

class Com extends Component {
  constructor(props){
    super(props);
    this.state={
      tel:'',
      username:'',
      holder:''
    }
  }
  componentDidMount(){
    let tel = JSON.parse(localStorage.getItem('user')).tel;
    let holder = JSON.parse(localStorage.getItem('user')).username;
    this.setState({
      tel: tel,
      holder:holder
    })
  }
  ChangeName(e) {
    let uname = e.target.value;
    this.setState({
      username: uname
    })
  }
  goBack(){
    this.props.history.push('/uesr')
  }
  Confim(){
    localStorage.setItem('user',JSON.stringify({
      tel:this.state.tel,
      username: this.state.username
    }));
    this.props.history.go(-1);
  }
  Logout(){
    localStorage.setItem('name','logouted');
    this.props.history.push('/');
  }
  render () {
    return (
      <div className="content">
        <div className="user_d_header">
          <div className="icon-twrap" p-id="2216"></div>
          <span onClick={this.goBack.bind(this)}>返回</span>
          <h1>个人信息</h1>
        </div>
        <div className="user_d_main">
          <div className="user_d_message">
            <h2>账号信息</h2>
            <div className="user_d_tel"><span>电话：</span>{this.state.tel}</div>
            <div className="user_d_name"><span>用户：</span><input type="text" id="user_name" name="user_name" placeholder={this.state.holder} onChange={this.ChangeName.bind(this)}/></div>
            <div className="user_d_avater"><span>头像：</span>
            <ImagePickerExample />
				</div>
            <button className= "exit_btn" onClick = {this.Confim.bind(this)}>确认修改</button>
            <button className= "exit_btn" onClick={this.Logout.bind(this) }>退出登录</button>
          </div>
        </div>
      </div>
    )
  }
}

export default Com;
