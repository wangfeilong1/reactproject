import React, { Component } from 'react';
// import { NavLink, Route, BrowserRouter as Router } from 'react-router-dom';
import './User.scss'
import Login from '@/pages/Login.jsx'
class Com extends Component {
  constructor (props) {
    super(props);
    this.state = {
      username:'杰米4460363690',
      tel:'',
      timer: '',
      avater:"https://s1.c.meishij.net/images/default/tx2_8.png"
    }
  }
  componentDidMount(){
    if(localStorage.getItem('user') !== null){
      let usrname= JSON.parse(localStorage.getItem('user')).username;
      let utel= JSON.parse(localStorage.getItem('user')).tel;
     
      if(usrname !== ''){
        this.setState({
          username : usrname,
          tel:utel,
        })
      }
    }
    if(localStorage.getItem('avater') !== null) {
      let uavater= JSON.parse(localStorage.getItem('avater')).avater;
      if(uavater !== ''){
        this.setState({
          avater:uavater
        })
      }
    }
  }
  goSet(){
    this.props.history.push('/userapp/userdetail')
  }
  fn(shu){
    shu=setInterval(function(){
       clearInterval(shu)      
  },1)
  }
  render () {
    if(localStorage.getItem('name')==='tang'){
      this.fn(this.timer)
      return (
        <div className="tang">
          <div className = "user_content" id="con">
            <div className ="headers" id = "headers">
              <div className ="tou" onClick = { this.goSet.bind(this) } >
                <img src={this.state.avater} alt=""/>
              </div>
              <div className = "right_r">
                <p className = "pa">{this.state.username }</p>
                <p className = "pb">正在通往美食达人的路上...</p>
                <p className = "pc">{this.state.tel }</p>
              </div>
            </div>
            <div className = "nav">
              <div className = "nav_Left">
                <p>0</p> 关注
              </div>
              <span className = "spanl"></span>
              <span className = "spanr"></span>
              <div className = "nav_Zhong">
              <p>0</p> 粉丝
              </div>
              <div className = "nav_Right">
              <p>0</p> 菜谱
              </div>
            </div>
            <div className = "my_rong">
              <h3>荣誉勋章</h3>
              <ul>
                <li><img src="http://static.meishij.net/images/get3/ceshi/sousuo.png"  alt=""/></li>
                <li><img src="http://static.meishij.net/images/get3/ceshi/kouwei1.png"  alt=""/></li>
                <li><img src="http://static.meishij.net/images/get3/ceshi/caixi1.png"  alt=""/></li>
                <li><img src="http://static.meishij.net/images/get3/ceshi/zucai1.png"  alt=""/></li>
                <li><img src="http://static.meishij.net/images/get3/ceshi/gexing1.png"  alt=""/></li>
                <li><img src="http://static.meishij.net/images/get3/ceshi/zucai1.png"  alt=""/></li>
                <li><img src="http://static.meishij.net/images/get3/ceshi/gexing1.png"  alt=""/></li>
              </ul>
            </div>
            <div className = "jifen">
              <div className = "jfen_img">
              <img src="https://s1.c.meishij.net/wap7/images/myhome_jifen@3x.png" alt=""/></div>
              <span>积分商城</span>
              <div className = "jfen_Rig">
              <img src="https://s1.c.meishij.net/wap7/images/ms_publis_recipe_r@3x.png" alt=""/></div>
              <p>免费兑换</p>
            </div>
            <div className = "my_New">
              <img src="https://s1.c.meishij.net/wap7/images/myhome_xiaoxi@3x.png" alt=""/>
              <span>消息通知</span>
              <div className = "Right">
              <img src="https://s1.c.meishij.net/wap7/images/ms_publis_recipe_r@3x.png" alt=""/></div>
            </div>
            <div className = "my_release unify">
              <img src="https://s1.c.meishij.net/wap7/images/myhome_fabu@3x.png" alt=""/>
              <span>我的发布</span>
              <div className = "Right">
              <img src="https://s1.c.meishij.net/wap7/images/ms_publis_recipe_r@3x.png" alt=""/></div>
            </div>
            <div className = "my_collect unify">
              <img src="https://s1.c.meishij.net/wap7/images/myhome_shoucang@3x.png" alt=""/>
              <span>我的收藏</span>
              <div className = "Right">
              <img src="https://s1.c.meishij.net/wap7/images/ms_publis_recipe_r@3x.png" alt=""/></div>
            </div>
            <div className = "my_order unify">
              <img src="https://s1.c.meishij.net/wap7/images/myhome_dingdan@3x.png" alt=""/>
              <span>我的订单</span>
              <div className = "Right">
              <img src="https://s1.c.meishij.net/wap7/images/ms_publis_recipe_r@3x.png" alt=""/></div>
            </div>
            <div className = "my_us unify">
              <img src="https://s1.st.meishij.net/p2/20190112/20190112151958_101.png" alt=""/>
              <span>关于我们</span>
              <div className = "Right">
              <img src="https://s1.c.meishij.net/wap7/images/ms_publis_recipe_r@3x.png" alt=""/></div>
            </div>
          </div>
        </div>
      )
    } else {
      return (
        <Login  props = { this.props }/>
      )
    }
  }
}

export default Com
