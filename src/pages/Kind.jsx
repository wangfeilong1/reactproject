import React, {Component} from 'react';
import KindHead from "@/components/Kind/KindHead";
import KindTab from "@/components/Kind/KindTab";
import KindList from "@/components/Kind/KindList";
import api from '@/api/Kind';

class AppKind extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: []
        }
    }
    componentDidMount() {
        api.requireKindTitle().then(data => {
            this.setState({
                title: data.data
            })
        });
    }
    render() {
        return (
            <div className={"kind"}>
                <KindHead {...this.props}/>
                <KindTab>
                    {/*通过props传递给组建不同的值*/}
                    {this.state.title.map((item, index) => (
                        <KindList key={index}
                                  type={item}
                                  title={<li>
                                      <i className={"iconfont icon-leimupinleifenleileibie"}></i>
                                      <span>{item}</span>
               
                                        </li>}
                                  {...this.props}
                        />
                        ))
                    }
                </KindTab>
            </div>
        );
    }
}

export default AppKind;
