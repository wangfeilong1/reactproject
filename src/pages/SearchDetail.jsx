import React, {Component} from 'react';
import './SearchDetail.scss';
import DetailHead from "@/components/Detail/DetailHead";
import ZaocanList from '@/components/zaocan/ZaocanList';
import api from '@/api/search';

class searchDetail extends Component {
    constructor(props){
        super(props);
        this.state={
            flag:'关注',
            textColor:'#ff4c39',
            product:{}
        }
    }
    //请求数据
    componentDidMount() {
        let id=this.props.match.params.id;
        api.requestDetail(id).then(data=>{
            this.setState({
                product:data[0]
            })
        })
    }

    guanzhu(){
      this.setState({
        flag:'已关注',
        textColor:'#ccc'
      });
    }
    render() {
        return (
            <div className={"search-list-detail"}>
                {/*页面头部组件*/}
                <DetailHead {...this.props } />
                <div className={"search-detail-banner"}>
                    <div className={"search-detail-banner-img"} style={{backgroundImage: "url("+this.state.product.image+")"}}></div>
                    <div className={"search-detail-banner-title"}>
                        <h3>{this.state.product.title}</h3>
                        <h5>{ parseInt(Math.random()*10000) }人浏览</h5>
                        <div className={"search-detail-banner-title-zuoyong"}>
                            <span>防癌</span>
                            <span>美白美容</span>
                            <span>清热祛火</span>
                            <span>助消化</span>
                            <span>防癌</span>
                            <span>美白美容</span>
                            <span>清热祛火</span>
                            <span>助消化</span>
                            <span>防癌</span>
                            <span>美白美容</span>
                        </div>
                    </div>
                </div>
                <div className={"search-detail-user-content"}>
                    <div className={"search-detail-user-username"}>
                        <div className={"search-detail-user-content-name"}>
                            <img src="https://s1.st.meishij.net/user/173/81/t3832923_142718761933940.jpg" alt=""/>
                            <h4>食色</h4>
                            <h5>发布2136篇菜谱</h5>
                        </div>
                        <div
                            className={"search-detail-user-content-guanzhu"}
                            onClick={this.guanzhu.bind(this)}
                            style={{borderColor:this.state.textColor,color:this.state.textColor}}
                        >
                            {this.state.flag}
                        </div>
                    </div>
                    <p className={"search-detail-user-text"}>
                        洗手做{this.state.product.title}。好像很久都没有搞点儿什么{this.state.product.title}，直到一天想要试试去做{this.state.product.title}这道菜。忽然想起，日日一份{this.state.product.title}好像也没再出过什么新花样了。
                    </p>
                </div>
                <div className={"search-detail-pingfen"}>
                    <div className={"search-detail-pingfen-left"}>
                        <h5>评分
                            <span>
                                <i className={"iconfont icon-wuxing"} />
                                <i className={"iconfont icon-wuxing"} />
                                <i className={"iconfont icon-wuxing"} />
                                <i className={"iconfont icon-wuxing"} />
                                <i className={"iconfont icon-wuxing"} />
                                </span></h5>
                        <ul>
                            <li><i className={"iconfont icon-icon-test"} /><span>煮</span></li>
                            <li><i className={"iconfont icon-tangguohe-"} /><span>甜味</span></li>
                            <li><i className={"iconfont icon-shijian"} /><span>&lt;2小时</span></li>
                            <li><i className={"iconfont icon-reliang"} /><span>较低热量</span></li>
                        </ul>
                    </div>
                    <div className={"search-detail-pingfen-right"}>
                        <i className={"search-iconfont icon-qizhi"} />
                    </div>
                </div>
                <div className={"search-detail-zhanshi"}>
                    <p>{this.state.product.title}成品展示</p>
                    <img src={this.state.product.image} alt=""/>
                </div>
                <h1>为您推荐</h1>
                <ZaocanList/>
            </div>
        );
    }
}

export default searchDetail;
