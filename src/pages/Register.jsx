import React from 'react'
import { NavLink } from 'react-router-dom';
import { Toast, } from 'antd-mobile';
import axios from 'axios';
import './Register.scss'
import baseUrl from '@/api';


class Com extends React.Component {
  constructor (props) {
    super(props);
    console.log(props)
    this.state = {
      tel: "",
      passwords: ""
    }
  }
  changeTel(e){
    let tel = e.target.value;
    setTimeout(()=>{
      this.setState({
        tel: tel
    })
    },10)
  }
  changePassword(e){
    let upwd = e.target.value;
    this.setState({
      passwords: upwd
    });
  }
  goBack(){
    console.log('ok')
    this.props.history.push('/user')
    
  }
  registers () {
    let tel = this.state.tel;
    let password = this.state.passwords;
    new Promise((resolve, reject) => {
    axios.post(baseUrl + '/api/users/register', {
      tel: tel,
      password: password
    }).then(data => {
      if (data.message === 'success') {
        Toast.fail('注册失败 !!!', 1);
        this.props.history.push('/userapp/register')
      }  else {
        setTimeout(()=>{
          this.props.history.push('/userapp/login')
        },300)
        Toast.success('注册成功，即将跳到登录页面', 3);
        }
      })
    })
  }
  render () {  
    return (
        <div className = "zuce">
         <div className = "zucet">
           <button onClick={this.goBack.bind(this)}>返回</button>
           <p>注册美食杰</p>     
           <NavLink to = "/login" className="h4">登陆</NavLink> 
         </div> 
         <div className="neir">
          <p><input type="text"placeholder = '邮箱/QQ账号/手机号' className="form-control" name="username" id="uname" ref="uname" onChange={this.changeTel.bind(this)}/></p>
          <p><input type="password" placeholder = "请输入密码"   className="form-control" name="password" id="upwd" ref="upwd" onChange={this.changePassword.bind(this)}/></p>
         </div>
         <div className = "login zhu">
            <h3 onClick={ this.registers.bind(this) }>注册</h3>
            <span></span>
         </div>
         <div className = "logo ilo">
            <div className = "qq">
              <img src="https://s1.c.meishij.net/wap6/images/QQ_login@3x.png" alt=""/>
              <span>QQ登陆</span>
            </div>
            <div className = "xin">
              <img src="https://s1.c.meishij.net/wap6/images/weibo_login@3x.png" alt=""/>
              <span>
                微博登陆
              </span>
            </div>
          </div>
        </div>
         )  
  }
}

export default Com
