import React, { Component } from 'react';
import './Find.scss';
class Com extends Component {
  render () {
    return (
      <div className ="content">
        <div className="find_content">
          <div className="top_img">
            <img src={"https://s1.st.meishij.net/p2/20190304/20190304100105_897.jpg"} alt=""/>
          </div>
          <div className="Find_artical">
              <h1>惊蛰养生有讲究，这6种果蔬必不可少，护肝排毒效果佳</h1>
              <p>春夏秋冬，四季轮回，随着渐渐进入初春，天气也慢慢回暖了。惊蛰，是二十四节气中的第三个节气，代表着春天的来临以及万物复苏的开始；清淡的饮食在这个时节是非常重要的，重点在于要养肝护脾。今天小编就给大家推荐一些惊蛰养生时令食材吧~</p>
            <div className = "Find_articals">
              <h2 className="Find_title">/韭菜/</h2>
              <p>韭菜是常见的一种蔬菜，性温味辛，有驱寒散淤、壮阳滋阴的功效。春季的韭菜最好吃，民间有“春香，夏辣，秋苦，冬甜”之说。初春吃韭菜，既补肾温阳，又益肝健胃，所以营养学家都称春韭为“养肝第一菜”。韭菜还含有丰富的纤维素，比大葱和芹菜都高，可以促进肠道蠕动、预防肠癌的发生；同时又能减少对胆固醇的吸收，起到预防和治疗动脉硬化、冠心病等疾病的作用。</p>
              <p><strong>适宜人群：动脉硬化、冠心病、高血压、便秘患者</strong><br/></p>
              <p><strong>食用禁忌：热性病症的人不宜食用</strong></p>
            
              <div className="Find_share">
                <div className="Find_share_de">   
                  <div className="share_img">
                    <img src={"https://s1.st.meishij.net/r/169/156/2789169/s2789169_147524141026236.jpg?imageMogr2/gravity/Center/crop/328x328"} alt=""/>
                  </div>        
                  <div className="avaterbox"> 
                    <strong>韭菜炒蘑菇</strong>       
                    <div className="avater">
                      <img src={"https://s1.st.meishij.net/user/169/156/s2789169_40505.jpg"} alt=""/>
                    </div>     
                    <span >禾小荷</span>
                  </div>
                </div>

                  <div className="Find_share_de">
                    <div className="share_img">
                      <img src={"https://s1.st.meishij.net/r/231/00/2937731/s2937731_149378039077461.jpg?imageMogr2/gravity/Center/crop/328x328"} alt=""/>
                    </div>   
                    <div className="avaterbox">
                      <strong >韭菜炒鸡蛋</strong>            
                        <div className="avater">
                          <img src={"https://s1.st.meishij.net/user/231/00/s2937731_145293450298916.jpg"} alt=""/>
                        </div>
                        <span>度姐厨房vx：Hxge2015</span>
                    </div>
                  </div>
                </div>
            </div>


            <div className = "Find_articals">
              <h2 className="Find_title">/苋菜/</h2>
              <p>苋菜性凉味甘，含有多种营养素，是老百姓心中的“长寿菜”。苋菜具有清热利湿、养肝明目、解暑排毒等功效，对湿热所致痢疾和肝火上升所致的目赤肿痛有较好的辅助治疗作用。苋菜叶富含易被人体吸收的钙质，对牙齿和骨骼的生长可起到促进作用，对儿童的成长也有一定益处。</p>
              <p><strong>适宜人群：适合老年人、幼儿、妇女、减肥者食用</strong><br/></p>
              <p><strong>食用禁忌：脾胃虚寒者忌食；胃肠有寒气、易腹泻的人也不宜多食</strong></p>
            
              <div className="Find_share">
                <div className="Find_share_de">   
                  <div className="share_img">
                    <img src={"https://s1.st.meishij.net/r/216/197/6174466/s6174466_149802165173685.jpg?imageMogr2/gravity/Center/crop/328x328"} alt=""/>
                  </div>        
                  <div className="avaterbox"> 
                    <strong>蚝油苋菜</strong>       
                    <div className="avater">
                      <img src={"https://s1.st.meishij.net/user/216/197/s6174466_152224665646447.jpg"} alt=""/>
                    </div>     
                    <span >心随彧动</span>
                  </div>
                </div>

                  <div className="Find_share_de">
                    <div className="share_img">
                      <img src={"https://s1.st.meishij.net/r/61/229/57311/s57311_149801989059332.jpeg?imageMogr2/gravity/Center/crop/328x328"} alt=""/>
                    </div>   
                    <div className="avaterbox">
                      <strong >韭菜炒鸡蛋</strong>            
                        <div className="avater">
                          <img src={"https://s1.st.meishij.net/user/61/229/s57311_86451.jpg"} alt=""/>
                        </div>
                        <span>斯佳丽</span>
                    </div>
                  </div>
                </div>
            </div>


            <div className = "Find_articals">
              <h2 className="Find_title">/西红柿/</h2>
              <p>西红柿性寒味甘，含有丰富的维生素C、胡萝卜素、番茄红素等，具有平肝凉血、养阴生津、健脾养胃、清热解毒的功效，被称为“神奇的菜中之果”。西红柿所含糖类多半为果糖和葡萄糖，既易于吸收，又养心护肝。所含蕃茄素，有消炎利尿作用，对肾脏病患者尤有裨益。</p>
              <p><strong>适宜人群：肾脏病患者、风湿、痛风患者</strong><br/></p>
              <p><strong>食用禁忌：不宜生吃，尤其是脾胃虚寒及月经期间的妇女；不宜空腹吃；不宜吃未成熟的青色番茄；不宜长时高温加热</strong></p>
            
              <div className="Find_share">
                <div className="Find_share_de">   
                  <div className="share_img">
                    <img src={"https://s1.st.meishij.net/r/206/231/5120456/s5120456_148059047631168.jpg?imageMogr2/gravity/Center/crop/328x328"} alt=""/>
                  </div>        
                  <div className="avaterbox"> 
                    <strong>西红柿烧茄子</strong>       
                    <div className="avater">
                      <img src={"https://s1.st.meishij.net/r/09/192/2735509/s2735509_144196122294307.jpg?imageMogr2/gravity/Center/crop/328x328"} alt=""/>
                    </div>     
                    <span >吃好吃的喽aici</span>
                  </div>
                </div>

                  <div className="Find_share_de">
                    <div className="share_img">
                      <img src={"https://s1.st.meishij.net/r/09/192/2735509/s2735509_144196122294307.jpg?imageMogr2/gravity/Center/crop/328x328"} alt=""/>
                    </div>   
                    <div className="avaterbox">
                      <strong >西红柿牛腩</strong>            
                        <div className="avater">
                          <img src={"https://s1.st.meishij.net/user/206/231/s5120456_154052322534907.jpg"} alt=""/>
                        </div>
                        <span>elsa飞雪</span>
                    </div>
                  </div>
                </div>
            </div>

            <div className = "Find_articals">
            <h2 className="Find_title">/香蕉/</h2>
            <p>香蕉富含蛋白质、膳食纤维、维生素A、C、B1、钾等多种营养成分，具有促进肝细胞再生、修复肝细胞、增强免疫力、促进代谢等功效。据说肝炎患者每日食用香蕉可预防肝硬化。香蕉富含钾和镁，钾能防止血压上升和肌肉痉挛，镁则具有消除疲劳的效果，因此，香蕉是高血压患者的首选水果。香蕉还有促进肠胃蠕动，润肠通便，润肺止咳、清热解毒，助消化和滋补的作用。</p>
            <p><strong>适宜人群：老人、小孩、便秘、高血压、冠心病、动脉硬化者</strong><br/></p>
            <p><strong>食用禁忌：体质偏虚寒者、关节炎患和糖尿病患者、肾炎患者；空腹不宜吃、</strong></p>
          
            <div className="Find_share">
              <div className="Find_share_de">   
                <div className="share_img">
                  <img src={"https://s1.st.meishij.net/r/41/149/5474791/s5474791_148455118164232.jpg?imageMogr2/gravity/Center/crop/328x328"} alt=""/>
                </div>        
                <div className="avaterbox"> 
                  <strong>香蕉麦芬</strong>       
                  <div className="avater">
                    <img src={"https://s1.st.meishij.net/r/09/192/2735509/s2735509_144196122294307.jpg?imageMogr2/gravity/Center/crop/328x328"} alt=""/>
                  </div>     
                  <span >breadmum</span>
                </div>
              </div>

                <div className="Find_share_de">
                  <div className="share_img">
                    <img src={"https://s1.st.meishij.net/r/67/36/4071567/s4071567_150942467953181.jpg?imageMogr2/gravity/Center/crop/328x328"} alt=""/>
                  </div>   
                  <div className="avaterbox">
                    <strong >香蕉奶昔</strong>            
                      <div className="avater">
                        <img src={"https://s1.st.meishij.net/user/67/36/s4071567_146244343672325.jpg"} alt=""/>
                      </div>
                      <span>老方小雨</span>
                  </div>
                </div>
              </div>
          </div>

          <div className = "Find_articals">
          <h2 className="Find_title">/莴笋/</h2>
          <p>惊蛰节气吃莴笋，可以增强胃液和消化液的分泌，增进胆汁的分泌。莴笋中含的钾是钠的27倍，而且其中的含碘量也很高，对人体的基础代谢和体格发育有利；莴笋肉质细嫩，生吃热炒都可以；经常咳嗽的朋友们多吃莴笋叶可以平喘止咳。 </p>
          <p><strong>适宜人群：糖尿病和肥胖、高血压、失眠、儿童生长期；亦可解酒</strong><br/></p>
          <p><strong>食用禁忌：脾胃虚寒、腹泻者不宜食用；一般人不宜食用过多，否则会导致间歇性夜盲；女性经期慎食</strong></p>
        
          <div className="Find_share">
            <div className="Find_share_de">   
              <div className="share_img">
                <img src={"https://s1.st.meishij.net/r/107/203/2488357/s2488357_150424668922919.jpg?imageMogr2/gravity/Center/crop/328x328"} alt=""/>
              </div>        
              <div className="avaterbox"> 
                <strong>莴笋焖鸡</strong>       
                <div className="avater">
                  <img src={"https://s1.st.meishij.net/user/107/203/s2488357_150427624135930.jpg"} alt=""/>
                </div>     
                <span >小玉厨房</span>
              </div>
            </div>

              <div className="Find_share_de">
                <div className="share_img">
                  <img src={"https://s1.st.meishij.net/r/247/247/3249497/s3249497_149928317323132.jpg?imageMogr2/gravity/Center/crop/328x328"} alt=""/>
                </div>   
                <div className="avaterbox">
                  <strong >豉油莴笋</strong>            
                    <div className="avater">
                      <img src={"https://s1.st.meishij.net/user/247/247/s3249497_150282857188236.jpg"} alt=""/>
                    </div>
                    <span>高兴KX</span>
                </div>
              </div>
            </div>
        </div>

        <div className = "Find_articals">
        <h2 className="Find_title">/空心菜/</h2>
        <p>春季吃空心菜是很好的选择，空心菜不仅脆嫩多汁，味道鲜美，性凉味甘；所含的烟酸、维生素C等能降低胆固醇、甘油三酯，具有降脂减肥的功效；空心菜的粗纤维素的含量较丰富，具有促进肠蠕动、通便解毒作用。</p>
        <p><strong>适宜人群：肥胖、高胆固醇、高血压患者</strong><br/></p>
        <p><strong>食用禁忌：脾胃虚寒、腹泻者慎食</strong></p>
      
        <div className="Find_share">
          <div className="Find_share_de">   
            <div className="share_img">
              <img src={"https://s1.st.meishij.net/r/223/01/3375473/s3375473_143529861693452.jpg?imageMogr2/gravity/Center/crop/328x328"} alt=""/>
            </div>        
            <div className="avaterbox"> 
              <strong>炝炒空心菜</strong>       
              <div className="avater">
                <img src={"https://s1.st.meishij.net/user/223/01/s3375473_150174322202886.jpg"} alt=""/>
              </div>     
              <span >小萌小调调</span>
            </div>
          </div>

            <div className="Find_share_de">
              <div className="share_img">
                <img src={"https://s1.st.meishij.net/r/58/07/2939308/s2939308_146278890113384.jpg?imageMogr2/gravity/Center/crop/328x328"} alt=""/>
              </div>   
              <div className="avaterbox">
                <strong >蒜蓉空心菜</strong>            
                  <div className="avater">
                    <img src={"https://s1.st.meishij.net/user/58/07/s2939308_143454391593142.jpg"} alt=""/>
                  </div>
                  <span>静默成诗</span>
              </div>
            </div>
          </div>
      </div>
      <div className="find_zong">
      <p>惊蛰时节，早晚温差大，阴雨天气多，小伙伴们要做好防护措施预防感冒啊~</p>
      </div>
      <div className="find_ping">
        <h1>
          评论
        </h1>
        <div  className="find_user">
          <div className="find_tou">
            <img src={"https://s1.st.meishij.net/user/158/37/t13509408_155082402181781.jpg"} alt=""/>
          </div>
          <strong>杰米6031759998</strong>
          <span>6小时前</span>
        </div>
        <div className="find_say">
          <p>看来要去下菜市场了</p>
        </div>
        <div  className="find_user">
          <div className="find_tou">
            <img src={"https://s1.st.meishij.net/user/164/37/t3384414_142440504952275.jpg"} alt=""/>
          </div>
          <strong>柠檬1421683949</strong>
          <span>17小时前</span>
        </div>
        <div className="find_say">
          <p>都是健康食材，赞</p>
        </div>
        </div>
      
        </div>
    </div>
  </div>
    
    )
  }
}
export default Com
