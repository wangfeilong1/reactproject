import React, { Component } from 'react';
import api from '@/api/artical';
import ArticalList from '@/components/Artical/ArticalList';

class Com extends Component {
  constructor(props){
    super(props)
    this.state={
      detail:{}
    }
  }
  componentDidMount(){
    let id = this.props.match.params.id;
    api.requestDetail (id).then(data=> {
      
      this.setState({
        detail:data
      })
    })
  }
  render () {
    return (
      
      <div className ="content">
      <div className="articalDetail_header" style={{
        height: '.45rem',
        width: '100%',
        position: 'fixed',
        left: '0',
        top: '0',
        'zIndex': '1000'
      }}>
      
		<a href="/#/artical" style={{display: 'inline-block',
      verticalAlign:'top',
      height: '.3rem',
      padding: '0 .12rem',
      fontSize: '.14rem',
      color: '#fff',
      borderRadius: '.15rem',
      lineHeight: '.30rem',
      background: 'rgba(0,0,0,0.5)',
      position: 'absolute',
      left: '.15rem',
      top: '.15rem'}}>&lt; 文章精选</a>
		<a href="/#/search"  style={{
      position: 'absolute',right:'1.20rem',top:'.15rem',
      borderRadius:'50%',
      background: 'rgba(0,0,0,0.5) url(http://site.meishij.net/p2/20170417/20170417102345_314.png) center no-repeat',
      backgroundSize: '.16rem .16rem',
      width:'.3rem',
      height:'.3rem'}}> </a>
		<a href="/#/kind" style={{display: 'inline-block',
      verticalAlign: 'top',
      height: '.30rem',
      padding: '0 .12rem',
      fontZize: '.14rem',
      color: '#fff',
      borderRadius: '.15rem',
      lineHeight: '.30rem',
      background: 'rgba(0,0,0,0.5)',
      position: 'absolute',
      right: '.15rem',
      top: '.15rem' }}>菜谱分类 &gt;</a>
	    </div>

      <div style={{'fontSize':'.26rem','fontWeight':'800','lineHight':'.3rem','padding':'.4rem .1rem'}}>{ this.state.detail.title }</div>
      <p style={{'font':'100 .18rem/.28rem ""','padding':'.25rem .15rem'}} className ="articaldetail_content">{this.state.detail.content}</p>
      <img style={{'width':'100%','height':'auto','padding':'.25rem .15rem'}} src={this.state.detail.img} alt=""/>

      <p style={{'font':'100 .18rem/.28rem ""','padding':'.25rem .15rem'}} >据现代科学分析，每100克{ this.state.detail.title }中含有蛋白质2.2克、脂肪0.3克、糖类1.9克、钙160毫克、磷61毫克、铁8.5毫克，还含有胡萝卜素和其他多种B族维生素。{ this.state.detail.title }营养丰富，含有较多的钙，磷、铁及胡萝卜素、维生索C、维生素P等，长期以来既作食用，又作药用。芹菜性味甘凉，具有清胃涤痰，祛风理气，利口齿爽咽喉，清盱明目和降压的功效。此外，芹菜中含有丰富的挥发性芳香油，既能增进食欲、促进血液循环，还能起到醒脑提神的食疗效用。
          中医认为，{ this.state.detail.title }性甘凉，具有清热、利尿、降压、祛脂等功效。入药用，水煎饮服或捣汁外敷，可辅助治疗早期高血压、高血脂症、支气管炎、肺结核、咳嗽、头痛、失眠、经血过多、功能性子宫出血、小便不利、肺胃积热、小儿麻疹、痄腮等症。</p>
      <h1>为您推荐</h1>
      <ArticalList/>
      </div>
    )
  }
}

export default Com
