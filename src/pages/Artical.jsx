import React, { Component } from 'react';
import ArticalList from '@/components/Artical/ArticalList'
import './Artical.scss';

class Com extends Component {

  render () {
    return (
      <div className ="content">
      <h1 className="artical_title">
        为您推荐
      </h1>
        <ArticalList/>
      <div className="artical_big">
      <div className="artical_name">酒桌上的“醒酒菜”就是它，利尿醒酒，护肝养颜，知道的人太少</div>
      <div className="artical_img" ></div>
      <div className="artical_more">
        <div className="artical_view">
            <img src={"https://s1.st.meishij.net/p2/20181214/20181214154020_569.png"}alt=""/><span>84073</span><span>2019-02-09</span>
      </div>
    </div>
      </div>
      <ArticalList/>
      <div className="artical_big">
      <div className="artical_name">这菜，贫血的人要多吃，补血护眼最厉害，4块钱能炒一大盘！</div>
      <div className="artical_img" style={{background:'#f8f8f8 url(https://s1.st.meishij.net/rs/241/19/8567491/n8567491_149907457368011.jpg) center no-repeat','backgroundSize':'cover'}}></div>
      <div className="artical_more">
        <div className="artical_view">
            <img src={"https://s1.st.meishij.net/p2/20181214/20181214154020_569.png"}alt=""/><span>89033</span><span>2019-03-09</span>
      </div>
    </div>
      </div>
      <ArticalList/>
      <div className="artical_big">
      <div className="artical_name">胡萝卜最上瘾的做法，健康香甜，3分钟就上桌，比炒肉好吃1百倍</div>
      <div className="artical_img" style={{background:'#f8f8f8 url(https://s1.st.meishij.net/rs/106/232/3995606/n3995606_145862511343790.jpg) center no-repeat','backgroundSize':'cover'}}></div>
      <div className="artical_more">
        <div className="artical_view">
            <img src={"https://s1.st.meishij.net/p2/20181214/20181214154020_569.png"}alt=""/><span>56073</span><span>2019-01-11</span>
      </div>
    </div>
      </div>
      <ArticalList/>
      <div className="artical_big">
      <div className="artical_name">这菜是老公最爱，蒸一蒸，肉香扑鼻，每周都要吃，怎么吃都不腻！</div>
      <div className="artical_img" style={{background:'#f8f8f8 url(https://s1.st.meishij.net/rs/108/183/358358/n358358_151202980115282.jpg) center no-repeat','backgroundSize':'cover'}}></div>
      <div className="artical_more">
        <div className="artical_view">
            <img src={"https://s1.st.meishij.net/p2/20181214/20181214154020_569.png"}alt=""/><span>64973</span><span>2019-01-15</span>
      </div>
    </div>
      </div>
      </div>
    )
  }
}
export default Com
