import React, { Component } from 'react';
import './Search.scss';
import { Link } from 'react-router-dom';
import api from '@/api/search'
import ZaocanList from '@/components/zaocan/ZaocanList';

class Com extends Component {
  constructor(props) {
    super(props);
    this.state={
      value:'',
      list:[]
    }
  }
  doSearch(){
    let sousuo = document.getElementById('sousuo').value;
    this.setState({value: sousuo})
      setTimeout(() => {
        let title = this.state.value;
        api.requestData (title).then((data=> {
        this.setState({
        list:data.data
        })
        console.log(this.state.list)
      }))
    }, 100);
  }
  render () {
    let html = '';
    if(this.state.list.length === 0 ){
      html = <div>
      <div className="search_header">
      <a href="/#/home" className="back"><i></i></a>
        <div className="search_container">
					<input id="sousuo" type="text" placeholder="菜谱名、食材名" />
					<input type="submit" value="搜索" onClick={this.doSearch.bind(this)}/>
        </div>
      </div>
      <h1 className="search_not_find" style={{fontSize:'.18rem','marginBottom':'1rem','padding':'.1rem'}}>不知道想吃啥？试试下面的推荐吧！</h1>
      <h2 className="search_youlike" style={{'padding':'.1rem'}}>猜你喜欢 <img style={{'width':'.18rem','height':'auto'}} src="https://s1.c.meishij.net/wap7/images/home_title_love@3x.png" alt=""/></h2>
      <ZaocanList/>
      </div>
    } else {
      html=<div>
      <div className="search_header">
      <a href="/#/home" className="back"><i></i></a>
        <div className="search_container">
					<input id="sousuo" type="text" placeholder="菜谱名、食材名" />
					<input type="submit" value="搜索" onClick={this.doSearch.bind(this)}/>
        </div>
      </div>
      <h1 style={{'padding':'.1rem'}}>为您找到{this.state.list.length}道关于{this.state.value}的菜！</h1>
      <ul className="search_list">
      {
        this.state.list.map((item, index) => {
          return (
          <Link className="tosSearchDetail" to = { '/detail/searchDetail/' + item.id } key = {item.id}>
          <li key = {item.id}>
          <div className="search_content">
          <img className="searchImg" src={ item.image } alt={item.image}/>
            <div className="searchInfo">
              <p>{item.title}</p><br/>
            <div className="search_view">
            <div className="stars"><i></i><i></i><i ></i><i></i><i></i></div>
              <span>{item.pingjia}</span>
            </div>
            </div>
            </div>
            </li>
            </Link>
          )
        })
      }
      </ul>
      </div>
    }
    return (
      <div className ="content">{html}</div>
    )
  }
}
export default Com
