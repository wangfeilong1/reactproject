import React, { Component } from 'react'
import { NavLink } from 'react-router-dom';
import axios from 'axios';
import  './Login.scss'
import { Toast, } from 'antd-mobile';
import baseUrl from '@/api';

class Com extends Component {
  constructor(props){
      super(props);
      console.log(this)
      this.state= {
        tel: '',
        password:''
      }

  }

  changeUsername(e){
    let tel = e.target.value;
    this.setState({
        tel: tel
    });
    //console.log(this.state.userName);
}
changePassword(e){
  let upwd = e.target.value;
  this.setState({
      Password: upwd
  });
  //console.log(this.state.userPassword);
}
goBack(){
  this.props.props.history.push('/')
  
}
  login () {
    new Promise((resolve, reject) => {
    axios.post(baseUrl+'/api/users/login', {
      
    }).then(data => {
      console.log(data)
      if (data.code === 404) {
        Toast.fail('登陆失败 !!!', 1);
        window.location.reload(true)
      }  else {
        window.location.reload(true) 
       setTimeout(()=>{
          this.props.history.push('/user')
        },2000)
        Toast.success('登陆成功，即将跳到用户页面', 2);
        
        localStorage.setItem('user',JSON.stringify({'tel':this.state.tel}));
        localStorage.setItem("name","tang");
      }
    })
  })
}

  render () {
    return (  
      <div className = "denglu">
        <header className = "dead">
        <div className = "tous">
          <button onClick={this.goBack.bind(this)}>返回</button>
          <h3>登陆美食杰</h3>
          <NavLink to = "/userapp/register"  className = "dian">注册</NavLink>
          </div>
          <div className = "shuru">
          <p><input type="text"placeholder = '请输入输入手机号/邮箱'  className="form-control" name="username" id="uname" ref="uname" onChange={this.changeUsername.bind(this)}/></p>
          <p><input type="password" placeholder = "请输入密码"  className="form-control" name="password" id="upwd" ref="upwd" onChange={this.changePassword.bind(this)}/></p>
        </div>
        <div className = "login">
            <h3 onClick={ this.login.bind(this) }>登陆</h3>
            <span>忘记密码?</span>
        </div>
        <div className = "logo">
            <div className = "qq">
            
              <img src="https://s1.c.meishij.net/wap6/images/QQ_login@3x.png" alt=""/>
              <span>QQ登陆</span>
            </div>
            <div className = "xin">
              <img src="https://s1.c.meishij.net/wap6/images/weibo_login@3x.png" alt=""/>
              <span>
                微博登陆
              </span>
            </div>
            
        </div>
        </header>
      </div>
    )
  }

}

export default Com
