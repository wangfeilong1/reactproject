import React, { Component } from 'react';
import ZcList from '@/components/zaocan/ZaocanList'
import './Zaocan.scss';

class Com extends Component {
  goSearch () {
    this.props.history.push('/search');
  }
  render () {
    return (
      <div className ="content">
        <div className="topbar">
        <a href="/#/home" className="logo"><img src={"//s1.st.meishij.net/p2/20180816/20180816120753_729.png"} alt="美食杰"/></a>
        <form method="get" action="/search">
          <div className="fbg"><input type="input" placeholder="搜索你想找的菜" name="title" /><input type="submit" value="搜索"  onClick={this.goSearch.bind(this)} /></div>
        </form>
        <a href="/#/kind" title="菜谱大全" className="link">菜谱大全</a>
        </div>

        <h1 className="zc_title">早餐</h1>
        <ZcList/>
      </div>
    )
  }
}
export default Com
