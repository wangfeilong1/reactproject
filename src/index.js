import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter as Router, Route, Switch } from 'react-router-dom';
import App from '@/layout/App';
import UserApp from '@/layout/UserApp';
import DetailApp from '@/layout/DetailApp';
import * as serviceWorker from './serviceWorker';
import './main.scss';
import 'antd-mobile/dist/antd-mobile.css'; 
import store from './store';

function renderFn() {
  ReactDOM.render(
    <Router>
      <Switch>
        <Route path="/detail" component = {DetailApp} />
        <Route path="/userapp" component = {UserApp} />
        <Route path="/" component = {App} />
      </Switch>
    </Router>,
  document.getElementById('root'));
}
renderFn();
store.subscribe(renderFn);

serviceWorker.unregister();
