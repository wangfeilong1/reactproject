import axios from 'axios';

import baseUrl from '../index'

const api = {
    requireKindTitle() {
        return new Promise((resolve, reject) => {
            axios({
                url: `${baseUrl}/api/all/caitype`
            })
                .then(res => resolve(res.data))
                .catch(err => reject(err))
        })
    },
    reuuireList(type) {
        return new Promise((resolve, reject) => {
            axios({
                url: `${baseUrl}/api/all/caitypeKind`,
                params: {caitype: type}
            })
                .then(res => {
                    // console.log(res.data);
                    resolve(res.data)
                })
                .catch(err => reject(err))

        })
    }
};

export default api;