import axios from 'axios';
import baseUrl from '@/api';

const api = {
  requestData (id) {
    return new Promise ((resolve, reject) => {
      axios.get(baseUrl + '/api/indexList/detail?id=' + id)
      .then(data => {
        resolve(data.data)
       
      }).catch (err => {
        reject(err)
      })
    })
  }
}

export default api
