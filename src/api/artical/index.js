import axios from 'axios';
import baseUrl from '@/api';

const api = {
  requestData () {
    return new Promise((resolve, reject) => {
      axios.get(baseUrl + '/api/articalList')
      .then(data => {
        resolve(data.data)
        
      })
      .catch(err => {
        reject(err)
      })
    })
  },
  requestDetail (id) {
    return new Promise ((resolve, reject) => {
      axios.get(baseUrl + '/api/articalList/detail?id=' + id)
      .then(data => {
        resolve(data.data[0])
      }).catch (err => {
        reject(err)
      })
    })
  }
}

export default api
