import axios from 'axios';
import baseUrl from '@/api';

const api = {
  requestData () {
    return new Promise((resolve, reject) => {
      axios.get(baseUrl + '/api/zaocan?pageNumber=20')
      .then(data => {
        resolve(data.data)
        
      })
      .catch(err => {
        reject(err)
      })
    })
  },
  requestDetail (id) {
    return new Promise((resolve, reject) => {
      axios.get(baseUrl + '/api/zaocan/detail?id='+ id)
      .then(data => {
        resolve(data.data[0])
       
      })
      .catch(err => {
        reject(err)
      })
    })
  }
}

export default api
