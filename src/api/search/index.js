import axios from 'axios';
import baseUrl from '@/api';

const api = {
  requestData (title) {
    return new Promise((resolve, reject) => {
      axios.get(baseUrl + '/api/all/titleSearch?title=' + title)
      .then(data => {
        resolve(data.data)
       
      })
      .catch(err => {
        reject(err)
      })
    })
  },
  requestDetail (id) {
    return new Promise ((resolve, reject) => {
      axios.get(baseUrl + '/api/all/searchDetail?id=' + id)
      .then(data => {
        resolve(data.data)
       
      }).catch (err => {
        reject(err)
      })
    })
  }
}

export default api
